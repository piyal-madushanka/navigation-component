package com.isprid.navigationcomponent.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.isprid.navigationcomponent.R
import com.isprid.navigationcomponent.validate
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*

class SignInFragment : Fragment(), View.OnClickListener {

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        view.btnViewTransactions.setOnClickListener(this)
        view.btnSendMoney.setOnClickListener(this)
        view.btnViewBalance.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnViewTransactions -> navController.navigate(R.id.action_mainFragment_to_viewTransactionsFragment)
            R.id.btnSendMoney ->{
              if(isValidForm()){
                  navController.navigate(R.id.action_mainFragment_to_chooseRecipientFragment)
              }
            }
            R.id.btnViewBalance -> navController.navigate(R.id.action_mainFragment_to_viewBalanceFragment)
        }
    }

    private fun isValidForm(): Boolean {
        val isEmail =
            et_sign_email.validate(
                getString(R.string.error_email_invalid)
            ) { s -> s.isValidEmail() }
        val isPassword =
            et_sign_password.validate(
                getString(R.string.error_password_length)
            ) { s -> s.length >= 8 }
        return isEmail && isPassword
    }

    fun String.isValidEmail(): Boolean = this.isNotEmpty() &&
            Patterns.EMAIL_ADDRESS.matcher(this.trim()).matches()
}