package com.isprid.navigationcomponent.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.isprid.navigationcomponent.R
import com.isprid.navigationcomponent.getString
import com.isprid.navigationcomponent.isValidEmail
import com.isprid.navigationcomponent.validate
import kotlinx.android.synthetic.main.fragment_view_balance.*
import kotlinx.android.synthetic.main.fragment_view_balance.view.*

class SignUpFragment :Fragment(), View.OnClickListener {
    private lateinit var navController: NavController
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_balance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        view.btnSignIn.setOnClickListener(this)
        view.btn_sign_up_ckj.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_sign_up_ckj->{
                if(isValidForm()){
                    navController.navigate(R.id.action_viewBalanceFragment_to_mainFragment)
                }
            }

            R.id.btnSignIn -> navController.navigate(R.id.action_viewBalanceFragment_to_mainFragment)
        }

    }

    private fun isValidForm(): Boolean {
        val isEmail =
           et_sign_up_email.validate(
                getString(R.string.error_email_invalid)
            ) { s -> s.isValidEmail() }
        val isPassword =
            et_sign_up_password.validate(
                getString(R.string.error_password_length)
            ) { s -> s.length >= 8 }
        val isPasswordConfirmed =
            et_sign_up_pas_re.validate(
                getString(R.string.error_password_not_matched)
            ) { s -> s == et_sign_up_pas_re.getString() }

        return isEmail && isPassword && isPasswordConfirmed
    }
}